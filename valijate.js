/**
 * Created by dsenevirathne on 10/27/2016.
 *
 */
var prev_error_alert_id = undefined;
(function () {
    $(document).ready(function () {
        valijate_fun();
    });
})(jQuery);

function validate_function(valijate_obj, element, id, can_leave_empty) {
    if (is_not_valid(valijate_obj, element, can_leave_empty))
        return mark_as_error(element[0], valijate_obj.err, id, flag);
    return mark_as_not_error(element[0], id, flag);
}

function radio_val(element, valijate_obj, block, flag, id) {
    var name = element.attr('name');
    if (!name)
        return;
    if (block) {
        flag.error = false;
        valijate_obj.outer_this.on('submit', function (e) {
            clear_messages(element[0], id);
            mark_as_not_error(element[0], id, flag);
            if (!valijate_obj.outer_this.children('input:radio[name=' + name + ']:checked').val()) {
                flag.error = true;
            }
            if (!flag.error)
                return;
            e.preventDefault();
            mark_as_error(element[0], valijate_obj.err, id, flag);
            flag.error = false;
        });
    }
}

function checkbox_val(element, valijate_obj, block, flag, id) {
    if (block) {
        flag.error = false;
        valijate_obj.outer_this.on('submit', function (e) {
            clear_messages(element[0], id);
            mark_as_not_error(element[0], id, flag);
            if (!element.is(':checked')) {
                flag.error = true;
            }
            if (!flag.error)
                return;
            e.preventDefault();
            mark_as_error(element[0], valijate_obj.err, id, flag);
            flag.error = false;
        });
    }
}

function vjbind(valijate_obj, element, id) {
    var at = [], block_submitting = true, can_leave_empty = true;

    if (!(isRadioOrCheckbox(element) && undefined == valijate_obj.con_arr))
        for (var i = 0; i < valijate_obj.con_arr.length; i++) {
            switch (valijate_obj.con_arr[i]) {
                case 'at_leave':
                    at.push('blur');
                    break;
                case  'at_text_change':
                    at.push('keyup');
                    break;
                case 'nblock':
                    block_submitting = false;
                    break;
                case 'nempty':
                    can_leave_empty = false;
                    break;
            }
        }
    var flag = {error: false};
    if (element.is(":checkbox")) {
        return checkbox_val(element, valijate_obj, block_submitting, flag, id);
    }
    if (element.is(":radio"))
        return radio_val(element, valijate_obj, block_submitting, flag, id);
    element.on('focus', function () {
        clear_messages(element[0], id);
    });
    valijate_obj.outer_this.on('submit', function () {
        if (is_not_valid(valijate_obj, element, can_leave_empty))
            return mark_as_error(element[0], valijate_obj.err, id, flag);
        return mark_as_not_error(element[0], id, flag);
    });
    element.on(at.join(' '), function () {
        if (is_not_valid(valijate_obj, element, can_leave_empty))
            return mark_as_error(element[0], valijate_obj.err, id, flag);
        else
            return mark_as_not_error(element[0], id, flag);
    });
    if (!can_leave_empty)
        element.on('blur', function () {
            if (is_text_empty(element))
                mark_as_error(element[0], valijate_obj.err, id, flag);
        });
    if (block_submitting) {
        valijate_obj.outer_this.on('submit', function (e) {
            if (!flag.error)
                return;
            e.preventDefault();
            show_alert(
                valijate_obj.outer_this.find(":submit").filter(":focus")[0] || valijate_obj.outer_this.find("input[type=submit]:focus")[0],
                "please correct above errors",
                'button_vj_alert_id_jfdlk21344532safl' + id);//this is a random and hard coded prefixed id which will be unique per each form
        });
    }
}

function is_text_empty(el) {
    return el.val() == "";
}

function is_not_valid(valijate_obj, element, can_leave_empty) {
    if ((valijate_obj.maxl !== undefined && element.val().length > valijate_obj.maxl) ||
        (valijate_obj.minl !== undefined && element.val().length < valijate_obj.minl))
        return true;
    if ((valijate_obj.gt || valijate_obj.st) && isNaN(Number(element.val())))
        return true;
    if ((valijate_obj.gt !== undefined && Number(element.val()) < valijate_obj.gt) ||
        (valijate_obj.st !== undefined && Number(element.val()) > valijate_obj.st))
        return true;
    if ((valijate_obj.maxwc !== undefined && element.val().split(" ").filter(function(x){return x != ""}).length > valijate_obj.maxwc) ||
        (valijate_obj.minwc !== undefined && element.val().split(" ").filter(function(x){return x != ""}).length < valijate_obj.minwc))
        return true;
    if (valijate_obj.must && !string_has(element.val(), valijate_obj.must))
        return true;
    if ((valijate_obj.sw && element.val().indexOf(valijate_obj.sw) != 0) ||
        (valijate_obj.ew && element.val().indexOf(valijate_obj.ew) != (element.val().length - valijate_obj.ew.length)))
        return true;
    if (valijate_obj.mustn && !string_does_not_have(element.val(), valijate_obj.mustn))
        return true;
    if (can_leave_empty != undefined && !can_leave_empty && is_text_empty(element))
        return true;
    return false;
}

function isRadioOrCheckbox(el) {
    return el.is(':checkbox') || el.is(':radio');
}


function valijate_fun() {
    start_message_styles();
    $(".valijate").each(function (index) {//search for each form
            var outer_this = $(this);
            $(this).find('input:not([type=button]), textarea').each(function (i) {
                if (undefined !== $(this).data("valijate") || isRadioOrCheckbox($(this))) {
                    var inner_this = $(this);
                    var conditions_array;
                    try {
                        conditions_array = string_to_array($(this).data("valijate"));
                        if (!isRadioOrCheckbox($(this))) {
                            if (!is_valid_array(conditions_array))
                                return;
                            if (!data_validate(inner_this))
                                return;
                        }
                        var valijate_obj = {
                            outer_this: outer_this,
                            con_arr: conditions_array,
                            err: inner_this.data('vjerr') || "Enter valid data",
                            tip: inner_this.data('vjtip') || "Enter valid data",
                            maxl: Number(inner_this.data('vjmaxl')),
                            minl: Number(inner_this.data('vjminl')),
                            gt: Number(inner_this.data('vjgt')),
                            st: Number(inner_this.data('vjst')),
                            must: string_to_array(inner_this.data('vjmust')),
                            mustn: string_to_array(inner_this.data('vjmustn')),
                            format: string_to_array(inner_this.data('vjformat')),
                            minwc: Number(inner_this.data('vjminwc')),
                            maxwc: Number(inner_this.data('vjmaxwc')),
                            sw: inner_this.data('vjsw'),
                            ew: inner_this.data('vjew')
                        }
                    }
                    catch (ex) {
                        return;
                    }
                    vjbind(valijate_obj, $(this), 'vj_' + index + '_' + i);
                }
            });
        }
    );
}

function is_valid_array(conditions_array) {
    /*the users data-valijate array should be validated
     * at_text_change and at_submit should not be in same array*/
    var valid_keyword_array = [
        'nblock',
        'nempty',
        'at_text_change',
        'at_leave'];
    for (condition in conditions_array)
        if (valid_keyword_array.indexOf(conditions_array[condition]) == -1) {
            console.error("" +
                "Valijate Error: invalid keyword " +
                conditions_array[condition] +
                " in valijate array. stopping valijate.");
            return false;
        }
    if (conditions_array.indexOf("at_text_change") != -1 &&
        conditions_array.indexOf("at_submit") != -1 &&
        conditions_array.indexOf("at_leave") != -1) {
        console.error("Valijate Error:Either 'at_text_change' or 'at_submit' removed. validating stopped.");
        return false;
    }
    return true;
}

function data_validate(el) {
    /*first of all, the max length and minimum length is checked.
     * whether those are numbers or not*/
    var arr = ['vjmaxl', 'vjminl'];
    var error = 'vjmust';
    for (var i = 0; i < 2; i++)
        if (undefined == el.data(arr[i]))
            continue;
        else if (isNaN(Number(el.data(arr[i])))) {
            console.error("Valijate Error: " + arr[i] + " should be a number, " +
                el.data(arr[i]) + " found.");
            return false;
        }
    /*max length value should be greater than of equal to minimum length*/
    if (Number(el.data('vjmaxl')) < Number(el.data('vjminl')))
        console.error("Valijate Error: max length should be a larger than minimum.");
    /* then grater than or smaller than values are checked.
     * whether those are numbers or not*/
    arr = ['vjgt', 'vjst'];
    for (i = 0; i < 2; i++)
        if (undefined == el.data(arr[i]))
            continue;
        else if (isNaN(Number(el.data(arr[i])))) {
            console.error("Valijate Error: " + arr[i] + " should be a number, " +
                el.data(arr[i]) + " found.");
            return false;
        }
    /*max length value should be greater than of equal to minimum length*/
    if (Number(el.data('vjgt')) > Number(el.data('vjst')))
        console.error("Valijate Error: vjgt should be a smaller than vgst.");
    arr = ['vjmaxwc', 'vjminwc'];
    for (i = 0; i < 2; i++)
        if (undefined == el.data(arr[i]))
            continue;
        else if (isNaN(Number(el.data(arr[i])))) {
            console.error("Valijate Error: " + arr[i] + " should be a number, " +
                el.data(arr[i]) + " found.");
            return false;
        }
    /*max length value should be greater than of equal to minimum length*/
    if (Number(el.data('vjminwc')) > Number(el.data('vjmaxwc')))
        console.error("Valijate Error: vjminwc should be a smaller than vjmaxwc.");
    /*must, mustn, and format data should be an array.*/
    var tmp = ['vjmustn', 'vjmust', 'vjformat'];
    try {
        for (i = 0; i < 3; i++) {
            if (undefined == el.data(tmp[i]))
                continue;
            if ((t = string_to_array(el.data(tmp[i]))).length == 1 && t[0] == "")//t created for temporary data storing
                continue;
        }
    } catch (ex) {
        console.error("Valijate Error: " + tmp[i] + " should be comma separated values. " + el.data(tmp[i]) + " found");
        return false;
    }
    return true;
}

function start_message_styles() {
    var style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = '.vj_error {position: absolute;color: #ffffff;z-index: 121;background-color: rgba(255, 19, 31, 0.57);padding: 5px;border-left: 5px solid black;width: auto;} ';
    style.innerHTML += '.vj_alert {position: absolute;color: #ffffff;z-index: 121;background-color: rgba(255, 0, 51, 0.57);padding: 5px;border-left: 5px solid black;width: auto;} ';
    document.getElementsByTagName('head')[0].appendChild(style);
}

function get_matching_regexp(symbol) {
    if (symbol.length == 2 && symbol[0] == '?')
        if (symbol[1] == 'n')
            return /\d+/g;
        else if (symbol[1] == 'l')
            return /[a-z]/i;
        else if (symbol[1] == 'c')
            return /[-!$%^&*()_+|~=`{}\[\]:";'<>?,.\/]/;
    return undefined;
}

function string_has(str, arr) {
    var regexp;
    for (i = 0; i < arr.length; i++) {
        regexp = get_matching_regexp(arr[i]);
        if (undefined == regexp && str.indexOf(arr[i]) != -1)
            continue;
        else if (undefined != regexp && str.match(regexp) != null)
            continue;
        else
            return false;
    }
    return true;
}

function string_does_not_have(str, arr) {
    var regexp;
    for (i = 0; i < arr.length; i++) {
        regexp = get_matching_regexp(arr[i]);
        if (undefined == regexp && str.indexOf(arr[i]) != -1)
            return false;
        if (undefined != regexp && str.match(regexp) != null)
            return false;
    }
    return true;
}

function string_to_array(str) {
    var arr, remove_last = false;
    if (str == undefined)
        return str;
    //if data has only one number, it's type is number and then it will not have string's functions and properties such as
    // indexOf, length. so, it is converted into a string by adding empty string
    str += '';
    //user may add only one condition, then ther will be no comma which will throw an exception while splitting
    //to avoid that, we have to add a comma at the end of the string
    if (str.length > 0 && str.indexOf(',') == -1) {
        str = str + ',';
        remove_last = true;
    }
    arr = str.split(",");
    if (remove_last)
        arr.splice(-1);
    if (arr[arr.length - 1] == "")
        arr.splice(-1);
    try {
        arr.forEach(function (item, index) {
            arr[index] = item.trim();
        });
    } catch (e) {
        throw "";
    }
    return eval("['" + arr.join("','") + "']");
}

function clear_messages(el, id) {
    $('#' + id).remove();
    if (prev_error_alert_id)
        $('#' + prev_error_alert_id).remove();
    prev_error_alert_id = undefined;
    el.style.border = "1px solid black";
}

/*
 * the error messages generating is done below
 * */

function mark_as_not_error(el, id, flag) {
    flag.error = false;
    clear_messages(el, id);
}

function mark_as_error(el, error_message, id, flag) {
    flag.error = true;
    show_message(el, error_message, id);
}

function show_message(el, error_message, id) {
    clear_messages(el, id);
    el.style.border = "1px solid red";
    var msg = document.createElement("P");
    msg.id = id;
    $(msg).addClass("vj_error");
    document.getElementsByTagName('body')[0].appendChild(msg);
    var a, b;
    var onScroll = function () {
        msg.style.top = el.getBoundingClientRect().bottom + window.scrollY + 'px';
        msg.style.left = el.getBoundingClientRect().left + window.scrollX + 'px';
    };
    window.removeEventListener('scroll', onScroll);
    window.addEventListener('scroll', onScroll);
    window.removeEventListener('resize', onScroll);
    window.addEventListener('resize', onScroll);
    onScroll();
    msg.style.width = 'auto';
    msg.innerHTML = error_message;
}

function show_alert(el, error_message, id) {
    prev_error_alert_id = id;
    $('#' + id).remove();//crearing alert, if there is already emmitted error.
    var msg = document.createElement("P");
    msg.id = id;
    $(msg).addClass("vj_alert");
    document.getElementsByTagName('body')[0].appendChild(msg);
    var a, b;
    var onScroll = function () {
        msg.style.top = el.getBoundingClientRect().bottom + window.scrollY + 'px';
        msg.style.left = el.getBoundingClientRect().left + window.scrollX + 'px';
    };
    window.removeEventListener('scroll', onScroll);
    window.addEventListener('scroll', onScroll);
    window.removeEventListener('resize', onScroll);
    window.addEventListener('resize', onScroll);
    onScroll();
    msg.style.width = 'auto';
    msg.innerHTML = error_message;
}

/*
 * validate parameters :
 *   nblock - abort submitting the form, if the conditions are not met
 *   nempty - user cannot leave it empty
 *   at_text_change - checks for conditions at each keystroke
 *   at_submit - validated at submit
 *   at_leave - validated at mouse leave
 *
 *   data-vjmsg: text - error message to be shown
 *   data-vjtip: text - tooltip to be shown
 *   data-vjmaxl:number - max length of the text
 *   data-vjminl:number - minimum length of the text
 *   data-vjgt:number - larger than this number
 *   data-vjst:number - smaller than this number
 *   data-vjmust:array of characters - must have characters
 *       * l - letter
 *       * n - number
 *       * c - character
 *       * any single character or text(ex: if user need 'T' and '.com' in texts, he can add 'em separated by commas)
 *   data-vjmustn:array of strings - characters that must not have (same notations above)
 *   data-vjformat:array of string - set of strings showing how the (ex:['*', '@', '*', '.', '*']
 *   */


/*UPTO NOW:
 *          valijate array is validated before processing.
 *  */